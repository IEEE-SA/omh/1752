## Intro to the survey schemas

Self-report measures of physical, mental, and social health, symptoms, wellbeing, and life satisfaction (Patient-Reported Outcomes, or PROs) and Ecological Momentary Assessments (EMAs) are gathered via sets of questions and answers. 

Surveys have been used for decades to gather data and a large number of them that have been validated in the field. The set of questions, value sets for their answers (where applicable) and use of their results are well documented (e.g., the Patient Health Questionnaire composed of 9 questions, or PHQ-9). Modeling a survey using a schema allows for consistent collection and pooling of result data. 

Given the vast number of standardized surveys and the need to create new ones to gather data on new outcomes of interest or to perform assessments on the field, survey schemas model the components of a survey to allow the creation of survey-specific schemas. The library includes also the schema of some example surveys that demonstrate specific application of the generic schemas.  

A survey schema models the set of questions and possible answers that make up the survey. The schema to model the survey will extend the **survey** schema 

### Schemas

The schemas model a survey as

*   an array of one or more **survey items**, where an item is a combination of question + possible answers [required]
*   the final survey score (calculated according to the survey codebook, usually by adding up the code of the answers) [optional]
*   a set of properties called "delivery details" [required], which include:
    *   survey start date time 
    *   survey end date time
    *   the end status of the survey: completed, abandoned (i.e., some answers provided), or missed (i.e., no answers provided)

While the "delivery details" property is required, the content of the set can be modified to fit specific survey delivery requirements (see below for more details). In particular, other properties can be added.  

```json
"properties": {
   "items": {
       "description": "The list of questions and answers that make up this survey.",
       "type": "array",
       "items": {
           "$ref": "#/definitions/survey_item"
       },
       "minItems": 1
   },
   "delivery_details": {
       "description": "The operational details of delivering this survey.",
       "type": "object",
       "properties": {
           "start_date_time": {
               "description": "The date time at which this survey was started.",
               "$ref": "#/definitions/date_time"
           },
           "end_date_time": {
               "description": "The date time at which this survey was finished (not necessarily completed).",
               "$ref": "#/definitions/date_time"
           },
           "end_status": {
               "description": "The way this survey was ended by the participant.",
               "type": "string",
               "enum": [
                   "abandoned",
                   "completed",
                   "missed"
               ]
           }
       }
   },
   "score": {
       "description": "The score calculated for the survey: this can be a number or a combination of numbers and/or string.",
       "oneOf": [
           {
               "type": "number"
           },
           {
               "type": "object"
           }
        ]
    }
}
```

The example schemas developed by the Sleep subgroup show how the extension of the **survey** schema is implemented 

The “definitions” section of each example schema includes:

*   the survey schema 
*   the survey item schema 
*   (other schemas, as explained later)

```json
"definitions": {
   "survey": {
       "$ref": "survey-1.0.json"
   },
   "survey_item": {
       "$ref": "survey-item-1.0.json"
   }
```

A survey item is a combination of question + possible answers. The **survey item** schema defines question, the array of answers and two [optional] timestamps:

*   asked date time 
*   answered date time

```json
"properties": {
   "question": {
       "description": "The question being asked.",
       "$ref": "#/definitions/question"
   },
   "answers": {
       "description": "A list of answers to the question. This list is empty if the question isn't answered and may have multiple elements if more than one answer is provided.",
       "type": "array",
       "items": {
           "$ref": "#/definitions/answer"
       }
   },
   "asked_date_time": {
       "description": "The date time at which this item was presented to the participant.",
       "$ref": "#/definitions/date_time"
   },
   "answered_date_time": {
       "description": "The date time at which the answer was provided. If multiple answers are provided, the suggestion is to use the last answer's date time.",
       "$ref": "#/definitions/date_time"
   }
}
```

To model the items of a specific survey, start by classifying them based on the types of answer, which can be, as defined in the **survey answer** schema:

```json
"type": [
   "object",
   "number",
   "string",
   "boolean"
]
```

The most common type of survey item is a question plus a set of predefined answers, called categories out of which the respondent can select only one answer, or one or more answers. To help in defining this type of item, the **survey categorical answer** schema extends the answer schema to add a code to the answer. 

If the survey includes items with this kind of answer, the relevant schema is referenced in the “definitions” section of the survey schema. 

```json
"definitions": {
   "survey": {
       "$ref": "survey-1.0.json"
   },
   "survey_item": {
       "$ref": "survey-item-1.0.json"
   },
   "survey_categorical_answer": {
       "$ref": "survey-categorical-answer-1.0.json"
   },
   "survey_categorical_answer_item": {
       "description": "A survey item that has one categorical answer.",
       "allOf": [
           {
               "$ref": "#/definitions/survey_item"
           },
           {
               "type": "object",
               "properties": {
                   "answers": {
                       "type": "array",
                       "items": {
                           "$ref": "#/definitions/survey_categorical_answer"
                       },
                       "maxItems": 1
                   }
               }
           }
       ]
   }
```

The last statement defines a survey item with a categorical answer for which only one answer is allowed.

If the same set of codes (categories) are used as answers to more than one question, it is more efficient to define the categories in the “definitions” section of the survey schema. 

Another common type of survey item is a question that expects an answer of type value + a unit of measure. To help in defining this type of item, the **survey unit value answer** schema extends the **survey answer schema** to model answers associated with numerical value(s) with a specific unit of measure. 

If the survey includes items with this kind of answer, the relevant schema is referenced in the “definitions” section of the survey schema

```json
"survey_unit_value_answer": {
   "$ref": "survey-unit-value-answer-1.0.json"
}
```

After the “definitions” section, the “allOf” clause specifies that the current schema uses all of the properties defined in the **survey schema**, with extensions that are specified inside the “properties” section to define the actual question + answer items.

```json
"allOf": [
   {
       "$ref": "#/definitions/survey"
   },
   {
       "properties": {
           "items": {
               "type": "array",
               "minItems": 10,
               "items": [
```

where “minItems” is the number of all possible questions that can be asked in the survey. In the case of, for example, the PHQ-9, this is 10. 

A **survey question** is composed of a text string and an optional label.

One important difference between a paper-based survey and a survey delivered on a mobile platform is that, since usually on the latter each question is asked on its own screen, each question must include all the necessary context information to allow the respondent to answer without the need to navigate out of the current screen. On a paper-based survey, such context information is usually provided only once, at the beginning, followed by all the questions on the same page.

#### Delivery Details 

As stated above, the schemas model a survey as

*   an array of one or more survey items (question + possible answers) [required]
*   the final survey score [optional]
*   a set of properties called "delivery details" [required], which include:
    *   survey start date time 
    *   survey end date time
    *   the end status of the survey: completed, abandoned (i.e., some answers provided) or missed (i.e., no answers provided)

While the "delivery details" property is required, the content of the set can be modified to fit specific survey delivery requirements. In particular, other properties can be added. For example:

*   a timestamp capturing when the survey was first delivered to the respondent can be defined as a separate piece of data from the timestamp capturing when the user started answering the survey
*   if the user is allowed to delay the start of the survey after it is first presented to her, the timestamp(s) related to this action and/or a boolean property can be defined 

If the "delivery details" property of a survey includes additional timestamps, the definitions section of the schema must include a reference to the date-time schema: 

```json"definitions": {
   "date_time": {
       "$ref": "date-time-1.0.json"
   },
   "survey": {
       "$ref": "survey-1.0.json"
   },
   "survey_item": {
       "$ref": "survey-item-1.0.json"
   }
```

Then the additional timestamp(s) can be defined: 

```json
"delivery_details": {
   "properties": {
       "first_delivered_date_time": {
           "$ref": "#/definitions/date_time"
       }
   }
}
```

### Instances

The survey schema expects instances to include:

*   all the questions that can possibly be asked (i.e., all those actually asked and those that could have been asked), and
*   for each question, all the answers provided by the respondent, or an empty array, if no answer was given

See the sample instances provided in the sample_data/survey folder for details.

### Summary

The generic schemas model a **survey** as an array of one or more survey items, plus a set of properties called "delivery details": start and end timestamps and the final status of the survey (completed, abandoned or missed). 

Each **survey item** is composed of a question and an answer, plus timestamps for when the question was asked and when it was answered. 

A **survey question** is composed of a text string and an optional label.

A **survey answer** is modeled as an array to account for situations in which more than one answer is allowed. A single answer can be enforced by setting the _maxItems_ property to 1.

The value of a **survey answer** can be of type object, number, string, or boolean.

Two schemas model two kinds of answers that are widely used: 

    The **categorical answer** schema models survey answers associated with a set of categories

    The **unit value answer schema** models answers associated with numerical value(s) with a specific unit of measure

**date answer** and **time answer** schemas are also provided 

While survey items are defined individually in the survey schema, common elements can be defined once and reused as needed. For example, if more than one answer references the same value set of possible answers, the latter should be defined once and then used as needed (see the sample instances provided in the sample_data/survey folder for examples). 

To ensure that the data complies with the survey schema, the property _minItems_ should be set to the number of questions in the survey.

The survey schema expects instances to include:

*   all the questions that can possibly be asked (i.e., all those actually asked and those that could have been asked), and
*   for each question, all the answers provided by the respondent, or an empty array, if no answer was given

### Examples

To illustrate how to use the generic survey schemas to implement a specific survey, the Sleep subgroup has created 4 example schemas modeling made-up surveys which are neither validated instruments nor proposed ones.
