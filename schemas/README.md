Scope
-----------------
The IEEE 1752.1™ standard defines specifications for the representation of mobile health minimum metadata, data related to sleep and physical activity. Data pertaining some environmental variables, surveys, and relevant units of measure and value sets are defined, because they are needed for the representation of minimum metadata, data related to sleep and physical activity. The standard is agnostic with respect to current or future sensing products that measure those types of health data. 

Mobile health (mHealth) data encompasses all varieties of personal health data that can be collected from equipment specific sensors (on the body, in the body, around the body) and mobile applications (within any mobile computing device).

Out of scope: The standard does not address the manner in which these schemas are transmitted, negotiated, encapsulated or any of the security or privacy solutions that may be required to create such transactions. 

Purpose
-----------------
The purpose is to provide standard semantics to enable meaningful description, exchange, sharing, and use of mobile health data related to sleep and physical activity. Data and associated metadata will be sufficiently clear and complete to support analysis for a broad set of consumer health, biomedical research, and patient-centered clinical care needs.

data-point and data-series
-----------------
A datapoint (data-point) is made up of a header and a body. The header of a data-point conforms to the header schema, and the body conforms to the schema relevant for the data being acquired or computed. 

A datapoint series (data-series) is made up of a header and a body holding an array of elements, each complying with the schema named in the header. The header of a datapoint series conforms to the header schema, and the array elements in the body conforms to the schema relevant for the data being acquired or computed. 

Organization
-----------------
- The organization of the IEEE 1752 Repository schemas into separate folders is for readability purpose only. Schemas that reference other schemas in the “definitions” section assume all schemas are located in the same directory. 
- The schemas in the repository are versioned using a major and minor version number, i.e., *schema-name*-X.y.json.
- The major portion of the version number shall change (e.g. 1.0 , 2.0) when a breaking change is made to the relevant schema (e.g., changing the datatype definition of an existing property AND / OR adding a required property)
- The minor portion of the version number shall change (e.g. 1.0 , 1.1) when a non-breaking change is made to the relevant schema (e.g., adding OR removing an optional property)
- In each schema, $id maps to the current version
- For semantic interoperability and depending on specific needs, each schema can be associated with one or more codes from a single standard vocabulary, or with codes from more than one standard vocabulary (e.g., SNOMED CT, LOINC, IEEE 11073-10101) by referencing a persistent URL for each code. 

```mermaid
graph TD
B(schemas)
  B --> E(environment)
click E "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/environment" "[environment fa:fa-link]"
  B --> F(metadata)
click F "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/metadata" "[metadata fa:fa-link]"
  B --> G(physical_activity)
click G "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/physical_activity" "[physical_activity fa:fa-link]"
  B --> H(sleep)
click H "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/sleep" "[sleep fa:fa-link]"
  B --> I(survey)
click I "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/survey" "[survey fa:fa-link]"
  B --> J(utility)
click J "https://opensource.ieee.org/omh/1752/-/tree/main/schemas/utility" "[utility fa:fa-link]"
    E --> K[ambient-light<br/>ambient-sound<br/>ambient-temperature]
    F --> L[data-point<br/>data-series<br/>header<br/>schema-id]
    G --> M[physical-activity]
    H --> N[apnea-hypopnea-index<br/>arousal-index<br/>deep-sleep<br/>light-sleep<br/>sleep-episode<br/>sleep-onset-latency<br/>sleep-stage-summary<br/>snore-index<br/>time-in-bed<br/>total-sleep-time<br/>wake-after-sleep-onset]
    I --> O[survey<br/>survey-answer<br/>survey-categorical-answer<br/>survey-date-answer<br/>survey-item<br/>survey-question<br/>survey-time-answer<br/>survey-unit-value-answer]	
		J --> X(body-posture<br/>date-time<br/>descriptive-statistic<br/>descriptive-statistic-denominator<br/>duration-unit-value<br/>duration-unit-value-range<br/>frequency-unit-value<br/>illuminance-unit-value<br/>kcal-unit-value<br/>length-unit-value<br/>percent-unit-value<br/>sound-unit-value<br/>speed-unit-value<br/>time-frame<br/>time-interval<br/>unit-value<br/>unit-value-range)
