# Contributor workflow

We welcome contributions from everyone and appreciate your consideration in helping us improve the project's schemas, sample data and documentation. 

## Contributor license agreement (CLA)

Please note that for any contribution to be accepted to this project, you must have already submitted an appropriate [Contributor License Agreement](https://opensource.ieee.org/community/cla/apache) to oscontrib@ieee.org and have received from IEEE staff a CLA number. 

This project is [licensed under the Apache License 2.0](https://opensource.ieee.org/omh/1752/-/blob/main/LICENSE). 
[Learn more](https://choosealicense.com/licenses/apache-2.0/).

# Contributing to IEEE 1752 repository

Thank you for taking the time to contribute.

Given the nature of this project, contributions can take various forms:

1. Provide suggestions for schema refinement based on new measures, biomarkers, sensor capabilities
2. Provide suggestions for documentation improvement 
3. Participate in the IEEE Working Group 

## Issue boards

If you are looking to find a way to contribute to our project, please first look over our issue boards to see if your suggestion has already been made by someone else and has already been labeled. 

If not, it is best to first [create an issue](https://saopen.ieee.org/creating-in-gitlab/#issue) and our project team will triage it by possibly requesting that you provide additional information as well as labeling it appropriately. Please consider including any mockups or screenshots if possible and applicable. 

If the project team solicits further work on the issue, see below the section about Merge Request

## IEEE 1752 Working Group

To participate in the IEEE Working Group (WG), please follow the instruction on the website to [join the listserv](https://sagroups.ieee.org/1752/) to receive emails with updates and invitations to the WG calls

Starting on July 6, 2021, the WG's activity will be driven by the Project Authorization Request [(PAR) P1752.2](https://sagroups.ieee.org/1752/)

## Merge request criteria 

We greatly appreciate it if your merge request met the following criteria

1. Your merge request should be associated with an issue that defines the bug your merge request fixes or the feature it adds. 
2. Your merge request should include a test or tests (please note that submitting tests that confirm a bug is a great contribution in and of itself even if you aren't able to provide a fix for a given bug)
3. Your merge request should update any relevant or impacted documentation
4. Try to keep the amount of changes you make in a single merge request as small as possible 

## Merge request workflow

Please use the following workflow when submitting merge requests: 

1. Fork our project into your personal namespace (or group)
2. Create a feature branch in your fork (if possible include the title of the issue or the issue number that the merge request corresponds to) 
3. Write any appropriate continuous integration tests. 
4. Push the commit(s) to your working branch in your fork.
  - squash commits into a small number of logically organized commits, keeping the commit history intact on shared branches
  - please try to have fewer than 15 commit messages per merge request 
5. Submit a merge request (MR) to our project's default branch 
  - merge request title should describe the change you wish to make
  - merge request description should give the reason for the change
  - use complete URLs to issues and other merge requests
  - include the complete path when referencing files within this repository (no URL required)

## Merge request acceptance criteria 

Your merge request needs approval from the 1752 Working Group. The 1752 site maintainers will let you know when to expect a response, depending on the maintainers' and Working Group's meeting schedules. 

### Commit messages guidelines

Please follow Chris Beam's seven rules for writing a good commit message: 

> 1. Separate subject from body with a blank line
> 2. Limit the subject line to 50 characters
> 3. Capitalize the subject line
> 4. Do not end the subject line with a period
> 5. Use the imperative mood in the subject line
> 6. Wrap the body at 72 characters
> 7. Use the body to explain what and why vs. how

For examples and additional explanation of these seven rules, please read Chris Beam's blog post: 
<https://chris.beams.io/posts/git-commit/>. 

**In addition, we ask that you also:**

* Use full urls to issues and merge requests (no short references, please) 
* Include the complete path when referencing files in your commit message body
* Do not include emoji in commit messages

## Communicating with us

Communication with the project team should be done through issues. Should you have additional questions, you can contact the IEEE 1752 Secretary at [simona@openmhealth.org] 

## Project governance and new maintainer process 

* List of project lead(s) with any affiliations that should be disclosed
  * Ida Sim, UCSF, 1752 Chair and Chair of P1752.1 subgroup on minimum metadata, SME 
  * Simona Carini, UCSF, 1752 Secretary
  * Anand Nandugudi, University of Memphis, OS site lead 

* List of project maintainers (including any affiliations), and if appropriate, organized to provide what area of work a given maintainer is in charge of (e.g. documentation, release, devops/testing, etc). 
  * Charlotte Chen, Philips, Chair of P1752.1 subgroup on sleep measures, SME 
  * Shiv Hiremath, Temple University, Chair of P1752.1 subgroup on physical activity measures, SME  

### Release Workflow

* Changes to the 1752 site need approval from the 1752 Working Group.
* The 1752 site maintainers triage issues and create a slate for the Working Group agenda and provide the necessary information on each issue and other proposals for change. 
* The 1752 Working Group votes on the proposed changes and the site maintainers implement the Working Group's decisions.
* The process above occurs quarterly.
* The 1752 Working Group meets virtually. Invitations are sent to the 1752 listserv. To [join the listserv](https://sagroups.ieee.org/1752/) follow the instructions. Participation is open to all. Voting is restricted to voting members. Voting membership is established according to the current 1752 Policies & Procedures 
* Slide deck and minutes of each 1752 Working Group call are posted on the [public website](https://sagroups.ieee.org/1752/meeting-agenda-minutes/)

We welcome participation and contributions to the project. 

**Requirements for becoming a new maintainer (preconditions)** 

* Candidates must agree to adhere to all applicable project and IEEE policies. 
* Candidates must have a valid and appropriate [Contributor License Agreement](https://opensource.ieee.org/community/cla/apache) on file with the IEEE SA. 
* Candidates should be familiar with the project and subject matter experts (SME's) and/or knowledgeable in JSON Schema and/or knowledgeable in GitLab processes.
* We do not allow for more than two maintainers to be from a single company or organization and strongly take into consideration any conflicts of interest amongst our candidates. 

**Process for becoming a new maintainer**
 
* Please [create a new issue](https://saopen.ieee.org/creating-in-gitlab/#issue) that you mark as confidential. 
* The Subject should be "New maintainer request: YOUR NAME" (substituting YOUR NAME with your preferred name). Include in the body of your issue a short background statement stating any affiliations or possible conflicts of interest. 
* This issue will be our primary way of communicating with you throughout this process. 
* When a vote is scheduled we will update the issue with any appropriate due date info and a comment 
* You may receive questions via this issue; please note that only maintainers and leads can access the issue as it is and will remain confidential
* Once a vote has been completed we will update the issue either welcoming you as a maintainer or rejecting you with or without explanation
 
**Right of appeal**
 
You have the right to appeal the process; we ask that you email opensource@ieee.org stating your wish to the IEEE SA Open Source Community manager to review that the process was followed properly and fairly.   

## Code of Conduct

Note that we have a code of conduct, please follow it in all your interactions with the project.
This project adheres to [IEEE Code of Conduct](https://www.ieee.org/content/dam/ieee-org/ieee/web/org/about/ieee_code_of_conduct.pdf) and adds the following

